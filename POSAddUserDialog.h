#ifndef POSADDUSERDIALOG_H
#define POSADDUSERDIALOG_H

#include <QDialog>
#include <QLineEdit>

namespace Ui {
  class POSAddUserDialog;
}

class POSAddUserDialog : public QDialog {
  Q_OBJECT

public:
  
  explicit POSAddUserDialog(QWidget *parent = 0);
  ~POSAddUserDialog();

public slots:

  void onNameEditEnter();
  void onIdEditEnter();
  void onKeyRecieved(const QString& label, bool);
  void on_generateBox_stateChanged(int enabled);
  
private:

  Ui::POSAddUserDialog *ui;
  QString generateId();
  QLineEdit* currentEdit;
};

#endif // POSADDUSERDIALOG_H
