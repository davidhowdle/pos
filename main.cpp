#include "POSMainWindow.h"
#include "POSDatabase.h"
#include <QApplication>

int main(int argc, char *argv[]) {
  QApplication a(argc, argv);
  POSMainWindow w;
  w.setWindowState(w.windowState() ^ Qt::WindowFullScreen);
  w.show();
  return a.exec();
}
