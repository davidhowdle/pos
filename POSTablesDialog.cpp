#include "POSTablesDialog.h"
#include "ui_POSTablesDialog.h"

POSTablesDialog::POSTablesDialog(QWidget *parent)
  : QDialog(parent),
    ui(new Ui::POSTablesDialog) {
  ui->setupUi(this);
}

POSTablesDialog::~POSTablesDialog() {
  delete ui;
}
