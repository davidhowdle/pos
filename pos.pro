#-------------------------------------------------
#
# Project created by QtCreator 2016-04-20T17:00:39
#
#-------------------------------------------------

QT       += core gui sql dbus

QMAKE_CXXFLAGS += -std=c++11

QT_DEBUG_PLUGINS = 1

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = pos
TEMPLATE = app

RESOURCES += resources.qrc

SOURCES += main.cpp \
           POSMainWindow.cpp \
           POSLoginDialog.cpp \
           POSTabContents.cpp \
           POSDatabase.cpp \
           POSSettingsDialog.cpp \
           POSKeyboard.cpp \
           POSAddUserDialog.cpp \
           POSClockThread.cpp
           
HEADERS += POSMainWindow.h \
           POSLoginDialog.h \
           POSTabContents.h \
           POSDatabase.h \
           POSSettingsDialog.h \
           POSKeyboard.h \
           POSAddUserDialog.h \
           POSClockThread.h

FORMS   += POSMainWindow.ui \
           POSLoginDialog.ui \
           POSTabContents.ui \
           POSSettingsDialog.ui \
           POSKeyboard.ui \
           POSAddUserDialog.ui

DESTDIR    = .
OBJECTS_DIR = .obj
MOC_DIR    = .moc
RCC_DIR    = .rcc
UI_DIR     = .ui
