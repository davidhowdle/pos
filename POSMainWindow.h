#ifndef POSMAINWINDOW_H
#define POSMAINWINDOW_H

#include <QMainWindow>
#include <QMap>
#include <QString>
#include <QLabel>

class POSLoginDialog;
class POSDatabase;
class POSSettingsDialog;
#include "POSClockThread.h"

namespace Ui {
  class POSMainWindow;
}

class POSMainWindow : public QMainWindow {
  Q_OBJECT

public:
  explicit POSMainWindow(QWidget *parent = 0);
  ~POSMainWindow();

public slots:

  void recieveLoginDetails(const QString& id, bool clockin);
  void logout();

private:

  void setupToolBar();

private: // Members

  Ui::POSMainWindow*  ui;
  POSDatabase*        db;
  POSLoginDialog*     login;
  POSSettingsDialog*  settings;
  POSClockThread      clockThread;
  QMap<QString, int>  tabMap;
  bool                hint;
  QAction*            logoutAction;
  QLabel*             timeLabel;
  QList<POSDatabase*> dbList;

private slots:

  void loginDialog();
  void settingsDialog();
  void poweroff();
  void toggleHint(bool isHint);
};

#endif // POSMAINWINDOW_H
