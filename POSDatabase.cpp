#include "POSDatabase.h"

#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlField>
#include <QVariant>
#include <QString>
#include <QSqlError>
#include <QDebug>
#include <QApplication>
#include <QMessageBox>

#include <iostream>

POSDatabase::POSDatabase(const std::string& host, const std::string& name)
  : QObject(nullptr) {

  database = QSqlDatabase::addDatabase("QPSQL");
  // Set the host name or IP of the server, and name
  database.setHostName(QString::fromStdString(host));
  database.setDatabaseName(QString::fromStdString(name));

  // Set the user
  database.setUserName("pos");

  // Set the password
  database.setPassword("postest");

  // Present a critical failure if either db cannot be opened
  // on construction
  bool ok = database.open();
  if (!ok) {
    QString message = "Database " + QString::fromStdString(name)
      + " could not be accessed " + "on host " + QString::fromStdString(host) + ": "
      + database.lastError().text()
      + "<br> Functions will not be available.  Quiting application";
    QMessageBox msg(QMessageBox::Critical, "", message,
                    QMessageBox::Ok, 0, Qt::FramelessWindowHint);
    msg.exec();
    qApp->quit();
  }
  // If connection is good, hook up signals for notifiers
  database.driver()->subscribeToNotification("test");
  connect(database.driver(), SIGNAL(notification(const QString&)), this, SLOT(notify(const QString&)));
}

POSDatabase::~POSDatabase() {
  database.close();
}

QSqlDatabase POSDatabase::getDatabase() {
  return database;
}

bool POSDatabase::exists(const QString& table,
    const QString& data,
    const QString& title) {
  QSqlQuery query(database);
  QString queryString = "select * from " + table;
  query.exec(queryString);
  int col = query.record().indexOf(title);
  while (query.next()) {
    if (query.value(col).toString() == data) return true;
  }
  return false;
}

bool POSDatabase::hasVal(const QString& table,
    const QString& data,
    const QString& dataTitle,
    const QString& value,
    const QString& valTitle) {
  QSqlQuery query(database);
  QString queryString = "select * from " + table;
  query.exec(queryString);
  int dataCol = query.record().indexOf(dataTitle);
  int valCol  = query.record().indexOf(valTitle);
  while (query.next()) {
    if (query.value(dataCol).toString() == data) {
      if (query.value(valCol).toString() == value) return true;
    }
  }
  return false;
}

QString POSDatabase::fetch(const QString& table,
    const QString& data,
    const QString& dataTitle,
    const QString& valTitle) {
  QSqlQuery query(database);
  QString queryString = "select * from " + table;
  QString ret = "";
  query.exec(queryString);
  int dataCol = query.record().indexOf(dataTitle);
  int valCol  = query.record().indexOf(valTitle);
  while (query.next()) {
    if (query.value(dataCol).toString() == data) {
      ret = query.value(valCol).toString();
    }
  }
  return ret;
}

QStringList POSDatabase::fetchEntry(const QString& table,
    const QString& title,
    const QString& data) {
  QSqlQuery query(database);
  QString queryString = "select * from " + table;
  QStringList ret;
  query.exec(queryString);
  int col = query.record().indexOf(title);
  while (query.next()) {
    if (query.value(col).toString() == data) {
      QSqlRecord record = query.record();
      for (int i = 0; i < record.count(); ++i) {
        ret << query.value(i).toString();
      }
    }
  }
  return ret;
}

QStringList POSDatabase::fetchAllCol(const QString& table,
    const QString& title) {
  QSqlQuery query(database);
  QString queryString = "select " + title + " from " + table;
  QStringList ret;
  query.exec(queryString);
  while (query.next()) {
    QSqlRecord record = query.record();
    for (int i = 0; i < record.count(); ++i) {
      ret << query.value(i).toString();
    }
  }
  return ret;
}

QVector<QStringList> POSDatabase::fetchAll(const QString& table) {
  QSqlQuery query(database);
  QString queryString = "select * from " + table;
  QVector<QStringList> all;
  query.exec(queryString);
  while (query.next()) {
    QStringList list;
    for (int i = 0; i < query.record().count(); ++i) {
      list << query.record().value(i).toString();
    }
    all.push_back(list);
  }
  return all;
}

QStringList POSDatabase::fetchHeader(const QString& table) {
  QSqlQuery query(database);
  QString queryString = "select * from " + table;
  QStringList headers;
  query.exec(queryString);
  while (query.next()) {
    for (int i = 0; i < query.record().count(); ++i) {
      headers << query.record().field(i).name();
    }
  }
  return headers;
}

void POSDatabase::notify(const QString& notice) {
  QString message = "Database notification: " + notice;
  QMessageBox msg(QMessageBox::Information, "", message,
      QMessageBox::Ok, 0, Qt::FramelessWindowHint);
  msg.exec();  
}
