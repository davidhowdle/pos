#include "POSSettingsDialog.h"
#include "ui_POSSettingsDialog.h"

#include "POSDatabase.h"

#include <iostream>

POSSettingsDialog::POSSettingsDialog(POSDatabase* db, QWidget *parent)
  : QDialog(parent),
    ui(new Ui::POSSettingsDialog),
    addUserDialog(new POSAddUserDialog(0)) {
  ui->setupUi(this);
  database = db;
  ui->tabWidget->removeTab(1);
  ui->tabWidget->setTabText(0, "Users");
  this->updateUsers("userUpdate");
  database->getDatabase().driver()->subscribeToNotification("userUpdate");
  connect(database->getDatabase().driver(), SIGNAL(notification(const QString&)),
      this, SLOT(updateUsers(const QString&)));
}

POSSettingsDialog::~POSSettingsDialog() {
  delete ui;
}

void POSSettingsDialog::updateUsers(const QString& notification) {
  if (!notification.contains("userUpdate")) return;
  QVector<QStringList> names = database->fetchAll("users");
  QStringList headers        = database->fetchHeader("users");
  ui->tableWidget->setHorizontalHeaderLabels(headers);
  ui->tableWidget->setRowCount(names.size());
  int row = 0, col = 0;
  for (int i = 0; i < names.size(); ++i) {
    col = 0;
    for (int j = 0; j < names[i].size(); ++j) {
      QTableWidgetItem* item = new QTableWidgetItem(names[i][j]);
      item->setFlags(item->flags() ^ Qt::ItemIsEditable);
      ui->tableWidget->setItem(row, col, item);
      ++col;
    }
    ++row;
  }
}

void POSSettingsDialog::on_addButton_clicked() {
  addUserDialog->exec();
}

void POSSettingsDialog::on_removeButton_clicked() {

}

void POSSettingsDialog::on_modButton_clicked() {

}
