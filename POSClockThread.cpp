#include "POSClockThread.h"

#include <QTimer>
#include <QDateTime>

void POSClockThread::run() {
  QTimer timer;
  connect(&timer, SIGNAL(timeout()), this, SLOT(timerHit()));
  timer.setInterval(10);
  timer.start();
  exec();
  timer.stop();
}

void POSClockThread::timerHit() {
  QString newTime = QDateTime::currentDateTime().toString("hh:mm:ss, MM/dd/yy");
  if (lastTime != newTime) {
    lastTime = newTime;
    QDate current = QDate::currentDate();
    QDate legal = current.addYears(-21);
    newTime += " : 21 from " + legal.toString("MM/dd/yy");
    emit sendTime(newTime);
  }
}
