#include "POSLoginDialog.h"
#include "ui_POSLoginDialog.h"

#include <QMessageBox>
#include <iostream>

POSLoginDialog::POSLoginDialog(POSDatabase* database, QWidget *parent)
  : QDialog(parent),
    ui(new Ui::POSLoginDialog) {
  ui->setupUi(this);
  connect(ui->clear, SIGNAL(clicked()), ui->infoPanel, SLOT(clear()));
  db = database;
}

POSLoginDialog::~POSLoginDialog() {
  delete ui;
  delete db;
}

void POSLoginDialog::check(const QString& id) {
  if (!db->exists("users", id, "id")) {
    ui->infoPanel->clear();
    return;
  }
  bool isAdmin  = false;
  QString data  = db->fetch("users", id, "id", "name");
  QString admin = db->fetch("users", id, "id", "admin");
  if (admin == "1")
    isAdmin = true;
  QString user  = "User: " + data;
  if (isAdmin)
    user += " (Admin)";
  ui->infoPanel->setText(user);
}

void POSLoginDialog::on_cancel_clicked() {
  this->close();
}

void POSLoginDialog::on_login_clicked() {
  if (!db->exists("users", ui->idDisplay->text(), "id")) {
    QString message = "Invalid user ID: " + ui->idDisplay->text();
    QMessageBox msg(QMessageBox::Warning, "", message,
                    QMessageBox::Ok, this, Qt::FramelessWindowHint);
    msg.exec();
    ui->idDisplay->clear();
    ui->infoPanel->clear();
    return;
  }
  bool isAdmin  = false;
  QString admin = db->fetch("users", ui->idDisplay->text(), "id", "admin");
  if (admin == "1") isAdmin = true;
  emit sendLoginDetails(ui->idDisplay->text(), clockin);
  ui->idDisplay->clear();
  ui->infoPanel->clear();
  this->close();
}

void POSLoginDialog::on_num0_clicked() {
  if (ui->idDisplay->text().size() < 4)
    ui->idDisplay->setText(ui->idDisplay->text() + "0");
  this->check(ui->idDisplay->text());
}

void POSLoginDialog::on_num1_clicked() {
  if (ui->idDisplay->text().size() < 4)
    ui->idDisplay->setText(ui->idDisplay->text() + "1");
  this->check(ui->idDisplay->text());
}

void POSLoginDialog::on_num2_clicked() {
  if (ui->idDisplay->text().size() < 4)
    ui->idDisplay->setText(ui->idDisplay->text() + "2");
  this->check(ui->idDisplay->text());
}

void POSLoginDialog::on_num3_clicked() {
  if (ui->idDisplay->text().size() < 4)
    ui->idDisplay->setText(ui->idDisplay->text() + "3");
  this->check(ui->idDisplay->text());
}

void POSLoginDialog::on_num4_clicked() {
  if (ui->idDisplay->text().size() < 4)
    ui->idDisplay->setText(ui->idDisplay->text() + "4");
  this->check(ui->idDisplay->text());
}

void POSLoginDialog::on_num5_clicked() {
  if (ui->idDisplay->text().size() < 4)
    ui->idDisplay->setText(ui->idDisplay->text() + "5");
  this->check(ui->idDisplay->text());
}

void POSLoginDialog::on_num6_clicked() {
  if (ui->idDisplay->text().size() < 4)
    ui->idDisplay->setText(ui->idDisplay->text() + "6");
  this->check(ui->idDisplay->text());
}

void POSLoginDialog::on_num7_clicked() {
  if (ui->idDisplay->text().size() < 4)
    ui->idDisplay->setText(ui->idDisplay->text() + "7");
  this->check(ui->idDisplay->text());
}

void POSLoginDialog::on_num8_clicked() {
  if (ui->idDisplay->text().size() < 4)
    ui->idDisplay->setText(ui->idDisplay->text() + "8");
  this->check(ui->idDisplay->text());
}

void POSLoginDialog::on_num9_clicked() {
  if (ui->idDisplay->text().size() < 4)
    ui->idDisplay->setText(ui->idDisplay->text() + "9");
  this->check(ui->idDisplay->text());
}
