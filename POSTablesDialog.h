#ifndef POSTABLESDIALOG_H
#define POSTABLESDIALOG_H

#include <QDialog>

namespace Ui {
class POSTablesDialog;
}

class POSTablesDialog : public QDialog
{
    Q_OBJECT

public:
    explicit POSTablesDialog(QWidget *parent = 0);
    ~POSTablesDialog();

private:
    Ui::POSTablesDialog *ui;
};

#endif // POSTABLESDIALOG_H
