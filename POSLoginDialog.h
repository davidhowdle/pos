#ifndef POSLOGINDIALOG_H
#define POSLOGINDIALOG_H

#include "POSDatabase.h"
#include <QDialog>

namespace Ui {
  class POSLoginDialog;
}

class POSLoginDialog : public QDialog {
  Q_OBJECT

public:

  explicit POSLoginDialog(POSDatabase* database, QWidget *parent = 0);
  ~POSLoginDialog();

  void setClockin(bool ci) {
    clockin = ci;
  }

private:

  Ui::POSLoginDialog* ui;
  void check(const QString& id);
  bool clockin;
  POSDatabase* db;

private slots:
  // numpad
  void on_num0_clicked();
  void on_num1_clicked();
  void on_num2_clicked();
  void on_num3_clicked();
  void on_num4_clicked();
  void on_num5_clicked();
  void on_num6_clicked();
  void on_num7_clicked();
  void on_num8_clicked();
  void on_num9_clicked();

  void on_cancel_clicked();
  void on_login_clicked();

signals:

  void sendLoginDetails(const QString& id, bool ci);
};

#endif // POSLOGINDIALOG_H
