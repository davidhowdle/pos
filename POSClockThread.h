#ifndef POSCLOCKTHREAD_H
#define POSCLOCKTHREAD_H

#include <QThread>

class POSClockThread : public QThread {
  Q_OBJECT

signals:

  void sendTime(const QString& time);

private:

  void run();
  QString lastTime;

private slots:

  void timerHit();
  
};

#endif // POSCLOCKTHREAD_H
