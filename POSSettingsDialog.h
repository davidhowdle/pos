#ifndef POSSETTINGSDIALOG_H
#define POSSETTINGSDIALOG_H

#include "POSAddUserDialog.h"

#include <QDialog>

class POSDatabase;

namespace Ui {
  class POSSettingsDialog;
}

class POSSettingsDialog : public QDialog {
  Q_OBJECT

public:

  explicit POSSettingsDialog(POSDatabase* db, QWidget *parent = 0);
  ~POSSettingsDialog();

private:

  Ui::POSSettingsDialog *ui;

  POSDatabase* database;
  POSAddUserDialog* addUserDialog;
                       
private slots:

  void on_addButton_clicked();
  void on_removeButton_clicked();
  void on_modButton_clicked();
  void updateUsers(const QString& notification);
  
};

#endif // POSSETTINGSDIALOG_H
