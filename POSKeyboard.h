#ifndef POSKEYBOARD_H
#define POSKEYBOARD_H

#include <QWidget>
#include <QMap>
#include <QString>
#include <QPushButton>

namespace Ui {
  class POSKeyboard;
}

class POSKeyboard : public QWidget {
  Q_OBJECT

public:

  explicit POSKeyboard(QWidget *parent = 0);
  ~POSKeyboard();

public slots:

  void showKeyboard();
  void hideKeyboard();
  bool isVisible() const;
  
signals:

  void keyClicked(const QString& text, bool upper);
  
private:

  Ui::POSKeyboard *ui;
  QMap<QString, QPushButton*> keyboardButtons;
  bool isUpper;

private slots:

  void buttonClicked(const QString& label);
  
};

#endif // POSKEYBOARD_H
