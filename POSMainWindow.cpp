#include "POSMainWindow.h"
#include "ui_POSMainWindow.h"
#include "POSLoginDialog.h"
#include "POSTabContents.h"
#include "POSDatabase.h"
#include "POSSettingsDialog.h"
#include "POSKeyboard.h"

#include <QToolBar>
#include <QToolTip>
#include <QMessageBox>
#include <QIcon>
#include <QDebug>
#include <QDBusConnection>
#include <QDesktopWidget>

POSMainWindow::POSMainWindow(QWidget *parent)
  : QMainWindow(parent),
    ui(new Ui::POSMainWindow),
    db(new POSDatabase(std::string("localhost"), std::string("functional"))),
    login(new POSLoginDialog(db, this)),
    settings(new POSSettingsDialog(db, this)),
    hint(false),
    timeLabel(new QLabel) {
  ui->setupUi(this);
  setCentralWidget(ui->tabWidget);
  setWindowFlags(Qt::Window | Qt::FramelessWindowHint | Qt::CustomizeWindowHint);
  this->setupToolBar();
  ui->tabWidget->hide();
  ui->tabWidget->clear();
  login->setModal(true);
  login->setWindowFlags(login->windowFlags() | Qt::FramelessWindowHint);
  settings->setModal(true);
  settings->setWindowFlags(login->windowFlags() | Qt::FramelessWindowHint);
  connect(login, SIGNAL(sendLoginDetails(const  QString&, bool)),
          this,  SLOT(recieveLoginDetails(const QString&, bool)));
  if(!QDBusConnection::sessionBus().registerService("com.pos.importmethod")) {
    qWarning() << "Service failed"; 
  }
  POSKeyboard* keyboard = new POSKeyboard(0);
  QDBusConnection::sessionBus().registerObject("/VirtualKeyboard", keyboard,
      QDBusConnection::ExportAllSignals | QDBusConnection::ExportAllSlots);
}

POSMainWindow::~POSMainWindow() {
  clockThread.quit();
  clockThread.wait();
  delete ui;
  delete logoutAction;
}

void POSMainWindow::setupToolBar() {
  QToolBar* mainToolBar    = ui->toolBar;
  mainToolBar->setIconSize(QSize(64,64));
  QAction*  loginAction    = mainToolBar->addAction(QIcon(":/login.png"),    "");
  QAction*  clockinAction  = mainToolBar->addAction(QIcon(":/clockin.png"),  "");
  clockinAction->setData(QString("Clockin"));
  QAction*  settingsAction = mainToolBar->addAction(QIcon(":/settings.png"), "");
            logoutAction   = mainToolBar->addAction(QIcon(":/logout.png"),   "");
  QAction*  clockoutAction = mainToolBar->addAction(QIcon(":/clockout.png"), "");
  QWidget*  space          = new QWidget(this);
  space->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
  mainToolBar->addWidget(space);
  mainToolBar->addWidget(timeLabel);
  connect(&clockThread, SIGNAL(sendTime(const QString&)), timeLabel, SLOT(setText(const QString&)));
  clockThread.start();
  QAction*  poweroffAction = mainToolBar->addAction(QIcon(":/poweroff.png"), "");
  logoutAction->setEnabled(false);
  connect(poweroffAction, SIGNAL(triggered()), this, SLOT(poweroff()));
  connect(loginAction,    SIGNAL(triggered()), this, SLOT(loginDialog()));
  connect(clockinAction,  SIGNAL(triggered()), this, SLOT(loginDialog()));
  connect(logoutAction,   SIGNAL(triggered()), this, SLOT(logout()));
  connect(settingsAction, SIGNAL(triggered()), this, SLOT(settingsDialog()));
  QToolBar* baseBar        = ui->baseBar;
  baseBar->setIconSize(QSize(64,64));
  QAction*  hintAction     = baseBar->addAction(    QIcon(":/hint.png"),     "");
  hintAction->setCheckable(true);
  connect(hintAction, SIGNAL(toggled(bool)), this, SLOT(toggleHint(bool)));
}

void POSMainWindow::loginDialog() {
  QAction* action = dynamic_cast<QAction*>(sender());
  if (hint) {
    if (action && action->data().toString() == "Clockin") {
      QToolTip::showText(this->mapToGlobal(QCursor::pos()), "Clockin");
    } else {
      QToolTip::showText(this->mapToGlobal(QCursor::pos()), "Login");
    }
    return;
  }
  login->exec();
  if (action) {
    if (action->data().toString() == "Clockin")
      login->setClockin(true);
    else login->setClockin(false);
  }
}

void POSMainWindow::poweroff() {
  if (hint) {
    QToolTip::showText(this->mapToGlobal(QCursor::pos()), "Power off");
    return;
  }
  QString title   = "Shutdown";
  QString message = "Shutdown Point-of-Sales?";
  QMessageBox msg(QMessageBox::Question, title, message,
                  QMessageBox::Yes | QMessageBox::No, this,
                  Qt::FramelessWindowHint);
  int answer = msg.exec();
  switch (answer) {
    case QMessageBox::Yes: qApp->quit(); break;
    case QMessageBox::No:  break;
    default:               break;
  }
}

void POSMainWindow::recieveLoginDetails(const QString& id, bool clockin) {
  bool isAdmin  = false;
  QString name  = db->fetch("users", id, "id", "name");
  QString admin = db->fetch("users", id, "id", "admin");
  if (admin == "1") isAdmin = true;
  if (tabMap.find(name) != tabMap.end()) {
    QString message = "User " + name + " already has a tab open.";
    QMessageBox msg(QMessageBox::Warning, "", message,
                   QMessageBox::Ok, this, Qt::FramelessWindowHint);
    msg.exec();
    return;
  }
  POSTabContents* userTab = new POSTabContents(this);
  userTab->setClockin(clockin);
  int tabNumber = ui->tabWidget->addTab(userTab, name);
  tabMap.insert(name, tabNumber);
  if (tabMap.size() >= 1) ui->tabWidget->show();
  logoutAction->setEnabled(true);
}

void POSMainWindow::logout() {
  if (hint) {
    QToolTip::showText(this->mapToGlobal(QCursor::pos()), "Logout");
    return;
  }
  int tabNumber = ui->tabWidget->currentIndex();
  // We will do some additional checks here,
  // eg. does the user have tables - then ask to transfer
  //     is the user themselves logging out, or is it an admin
  //     if not refuse
  QString name = ui->tabWidget->tabText(tabNumber);
  ui->tabWidget->removeTab(tabNumber);
  if (ui->tabWidget->count() <= 0) logoutAction->setEnabled(false);
  auto itr = tabMap.find(name);
  if (itr == tabMap.end()) return;
  tabMap.erase(itr);
}

void POSMainWindow::settingsDialog() {
  if (hint) {
    QToolTip::showText(this->mapToGlobal(QCursor::pos()), "Settings");
    return;
  }
  settings->show();
}

void POSMainWindow::toggleHint(bool isHint) {
  this->hint = isHint;
}
