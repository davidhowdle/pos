#include "POSKeyboard.h"
#include "ui_POSKeyboard.h"

#include <QSignalMapper>
#include <QDebug>
#include <QDesktopWidget>

#include <iostream>

POSKeyboard::POSKeyboard(QWidget *parent)
  : QWidget(parent),
    ui(new Ui::POSKeyboard),
    isUpper(false) {
  ui->setupUi(this);
  setWindowFlags(Qt::WindowDoesNotAcceptFocus | Qt::Tool |
      Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
  QRect full = QApplication::desktop()->screenGeometry();
  QRect geom = geometry();
  int cX = full.bottomRight().x()/2 - geom.bottomRight().x()/2;
  int cY = full.bottomRight().y() - geom.bottomRight().y();
  QWidget::move(QPoint(cX, cY));
  keyboardButtons = {
    {"1", ui->num1}, {"2", ui->num2}, {"3", ui->num3},
    {"4", ui->num4}, {"5", ui->num5}, {"6", ui->num6},
    {"7", ui->num7}, {"8", ui->num8}, {"9", ui->num9},
    {"0", ui->num0}, {"<", ui->del}, {"enter", ui->enter},
    {"space", ui->space}, {"+", ui->plus}, {"-", ui->minus},
    {"q", ui->q}, {"w", ui->w}, {"e", ui->e},
    {"r", ui->r}, {"t", ui->t}, {"y", ui->y},
    {"u", ui->u}, {"i", ui->i}, {"o", ui->o},
    {"p", ui->p}, {"a", ui->a}, {"s", ui->s},
    {"d", ui->d}, {"f", ui->f}, {"g", ui->g},
    {"h", ui->h}, {"j", ui->j}, {"k", ui->k},
    {"l", ui->l}, {"z", ui->z}, {"x", ui->x},
    {"c", ui->c}, {"v", ui->v}, {"b", ui->b},
    {"n", ui->n}, {"m", ui->m}, {"shift", ui->shift}
  };
  QSignalMapper* mapper = new QSignalMapper(this);
  connect(mapper, SIGNAL(mapped(const QString&)), this, SLOT(buttonClicked(const QString&)));
  for (auto key : keyboardButtons.keys()) {
    mapper->setMapping(keyboardButtons.value(key), key);
    connect(keyboardButtons.value(key), SIGNAL(clicked()), mapper, SLOT(map()));
  }
}

POSKeyboard::~POSKeyboard() {
  delete ui;
}

void POSKeyboard::buttonClicked(const QString& label) {
  QVector<QString> forbid = {"shift", "space", "enter"};
  if (label == "enter") {
    hideKeyboard();
    for (auto itr = keyboardButtons.begin(); itr != keyboardButtons.end(); ++itr) {
      itr.value()->setText(itr.key().toLower());
    }
    auto shift = keyboardButtons.find("shift");
    if (shift == keyboardButtons.end()) return;
    shift.value()->setChecked(false);
    return;
  } else if (label == "shift") {
    if (isUpper) {
      isUpper = false;
      for (auto itr = keyboardButtons.begin(); itr != keyboardButtons.end(); ++itr) {
        itr.value()->setText(itr.key().toLower());
      }
    } else {
      isUpper = true;
      for (auto itr = keyboardButtons.begin(); itr != keyboardButtons.end(); ++itr) {
        if (forbid.contains(itr.key())) continue;
        itr.value()->setText(itr.key().toUpper());
      }
    }
    return;
  }
  emit keyClicked(label, isUpper);
}

void POSKeyboard::showKeyboard() {
  QWidget::show();
}

void POSKeyboard::hideKeyboard() {
  QWidget::hide();
}

bool POSKeyboard::isVisible() const {
  return QWidget::isVisible();
}
