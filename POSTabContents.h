#ifndef POS_TAB_CONTENTS_H
#define POS_TAB_CONTENTS_H

#include <QWidget>

namespace Ui {
  class POSTabContents;
}

class POSTabContents : public QWidget {
  Q_OBJECT

public:
  explicit POSTabContents(QWidget *parent = 0);
  ~POSTabContents();

  void setClockin(bool clockin) {
    clockedIn = clockin;
  }

private:
  Ui::POSTabContents *ui;
  bool clockedIn;

private slots:
  void on_newButton_clicked();
};

#endif // POS_TAB_CONTENTS_H
