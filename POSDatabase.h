#ifndef POS_DATABASE_H
#define POS_DATABASE_H

#include <QSqlDatabase>
#include <QSqlDriver>
#include <QStringList>
#include <QObject>

class POSDatabase : QObject {

  Q_OBJECT
  
public:

  POSDatabase(const std::string& host, const std::string& name);
  ~POSDatabase();

  QSqlDatabase         getDatabase();
  bool                 exists     (const QString& table,     const QString& data, const QString& title);
  bool                 hasVal     (const QString& table,     const QString& data,
                                   const QString& dataTitle, const QString& value,
                                   const QString& valTitle);
  QString              fetch      (const QString& table,     const QString& data,
                                   const QString& dataTitle, const QString& valTitle);
  QStringList          fetchEntry (const QString& table,     const QString& title, const QString& data);
  QStringList          fetchAllCol(const QString& table,     const QString& title);
  QVector<QStringList> fetchAll   (const QString& table);
  QStringList          fetchHeader(const QString& table);                                                       
                                                        
public slots:

  void                 notify     (const QString& notice);
  
private:

  QSqlDatabase         database;

};

#endif
