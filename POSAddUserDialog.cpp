#include "POSAddUserDialog.h"
#include "ui_POSAddUserDialog.h"

#include <iostream>
#include <random>
#include <sstream>

#include <QVector>
#include <QDBusConnection>
#include <QDBusMessage>
#include <QDebug>

POSAddUserDialog::POSAddUserDialog(QWidget *parent)
  : QDialog(parent),
    ui(new Ui::POSAddUserDialog) {
  ui->setupUi(this);
  setWindowFlags(Qt::Window | Qt::FramelessWindowHint | Qt::CustomizeWindowHint);
  setFocus();
  connect(ui->nameEdit, SIGNAL(cursorPositionChanged(int, int)), this, SLOT(onNameEditEnter()));
  connect(ui->idEdit, SIGNAL(cursorPositionChanged(int, int)), this, SLOT(onIdEditEnter()));
  if (!QDBusConnection::sessionBus().connect("com.pos.importmethod", "/VirtualKeyboard",
          "local.pos.POSKeyboard", "keyClicked", this, SLOT(onKeyRecieved(const QString&, bool)))) {
    std::cout << "Connection failed" << std::endl;
  }
  ui->idEdit->setEnabled(false);
  ui->idEdit->setText(this->generateId());
}

POSAddUserDialog::~POSAddUserDialog() {
  delete ui;
  delete currentEdit;
}

QString POSAddUserDialog::generateId() {
  std::random_device device;
  std::mt19937 generator(device());
  std::uniform_int_distribution<int> dist(0, 9);
  std::stringstream ss;
  for (int i = 0; i < 4; ++i) {
    ss << dist(generator);
  }
  return QString::fromStdString(ss.str());
}

void POSAddUserDialog::onNameEditEnter() {
  QDBusMessage msg = QDBusMessage::createMethodCall("com.pos.importmethod",
      "/VirtualKeyboard", "", "showKeyboard");
  QDBusMessage queued = QDBusConnection::sessionBus().call(msg);
  qApp->processEvents();
  currentEdit = ui->nameEdit;
}

void POSAddUserDialog::onIdEditEnter() {
  QDBusMessage msg = QDBusMessage::createMethodCall("com.pos.importmethod",
      "/VirtualKeyboard", "", "showKeyboard");
  QDBusMessage queued = QDBusConnection::sessionBus().call(msg);
  qApp->processEvents();
  currentEdit = ui->idEdit;
}

void POSAddUserDialog::onKeyRecieved(const QString& label, bool upper) {
  if (label.isEmpty()) return;
  QString currentLabel;
  if (upper) currentLabel = label.toUpper();
  else currentLabel = label.toLower();
  if (label == "<") currentEdit->backspace();
  else currentEdit->setText(currentEdit->text() + currentLabel);
}

void POSAddUserDialog::on_generateBox_stateChanged(int checked) {
  if (checked) {
    ui->idEdit->setEnabled(false);
    ui->idEdit->setText(this->generateId());
    return;
  }
  ui->idEdit->setEnabled(true);
  ui->idEdit->clear();
}
